import ballerinax/kafka;
import ballerina/io;
kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {

    string jsonFilePath = "./files/sample.json";
    string choice = io:readln("Which operation would you like to perform(read/write): ");

    if choice=="write" {
        io:println("Starting write operation..............");
        string key = io:readln("Enter key(studentNo): ");
        string value =io:readln("Enter value(StudentName): ");
        string age =io:readln("Enter value2(StudentAge): ");
        map<json> jsonContent ={
        studentName:'key,
        studentNo:value,
        studentAge:age
        };
        check io:fileWriteJson(jsonFilePath, jsonContent);
        io:println("WRITE OPERATION SUCCESSFUL , Json content " +jsonContent.toJsonString() +" was added to file "+  jsonFilePath);
        string message = jsonContent.toJsonString();
        check kafkaProducer->send({
                                topic: "dsar",
                                value: message.toBytes()});
             check kafkaProducer->'flush();
        }else if choice=="read" { io:println("Starting read operation...........................");
               json readJson = check io:fileReadJson(jsonFilePath);
                  io:println(readJson);
        }else {io:print("Invalid choice");
         }
}